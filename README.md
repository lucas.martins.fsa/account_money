# Account_Money

O trabalho consiste em implementar um pequeno sistema que emula um "Cofrinho
de moedas" em Java. Criando um menu em que é oferecido ao usuário:
- Adicionar moedas de diferentes valores e países em seu cofrinho
- Remover moedas específicas do cofrinho.
- Listar todas as moedas que estão dentro do cofrinho
- Calcular quanto dinheiro existe no cofrinho convertido para Real


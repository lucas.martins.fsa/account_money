package trabalho;

import java.util.ArrayList;

public class Cofrinho {

	static ArrayList<Moeda> listaMoeda = new ArrayList<Moeda>();

	public void adicionar(Moeda moeda) {
		listaMoeda.add(moeda);
	}

	public void remover(Moeda moeda) {
		listaMoeda.remove(moeda);
	}

	public void listar() {
		for (Moeda moeda : listaMoeda) {
			System.out.println(moeda);
		}
	}

	public void totalConvertido() {
		double total = 0;

		listar();

		for (Moeda moeda : listaMoeda) {
			total += moeda.converterEmReal();
		}

		System.out.println();
		System.out.println("Total no cofrinho é de R$ " + total + "");
	}

}
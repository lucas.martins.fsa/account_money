package trabalho;

public class Euro extends Moeda {
	
	//atributos da classe Euro
	public String nome = "Euro";
	public double peso = 5.16;
	
	//Metodo Construtor
	public Euro(double valor) {
		super(valor);
	}
	
	//Convertendo valor em string
	@Override
	public String toString() {
		return "Euro - Є " + valor + "";
	}
	
	//Metodo para comparação de atributos das moedas
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Euro other = (Euro) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (Double.doubleToLongBits(peso) != Double.doubleToLongBits(other.peso))
			return false;
		return true;
	}
	
	//Metodo para converter os valores em reais. o Override informa ao compilador que o elemento deve substituir um elemento declarado em uma superclasse
	@Override
	public double converterEmReal() {
		return valor * peso;
	}

}

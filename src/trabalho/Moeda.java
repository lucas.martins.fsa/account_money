package trabalho;

public abstract class Moeda {
	public double valor;
	
	//Metodo Contrutor
	public Moeda(double valor) {
		super();
		this.valor = valor;
	}
	
	//Metodo para comparação de atributos das moedas
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Moeda other = (Moeda) obj;
		if (Double.doubleToLongBits(valor) != Double.doubleToLongBits(other.valor))
			return false;
		return true;
	}
	
	//Metodo para conversão dos valores da classe em real. essa classe não tem peso para ser convertido, retornando somente o resultado já resolve
	abstract double converterEmReal();
}

package trabalho;

public class Dolar extends Moeda {
	//Atributos da classe Dolar
	public String name = "Dolar";
	public double peso = 5.40;
	
	//Metodo Construtor
	public Dolar(double valor) {
		super(valor);
	}
	
	//Conversão do valor em String
	@Override
	public String toString() {
		return "Dolar - US$ " + valor + "";
	}
	
	//Metodo para comparação de atributos das moedas
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dolar other = (Dolar) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(peso) != Double.doubleToLongBits(other.peso))
			return false;
		return true;
	}
	
	//Conversão do valor Dolar em Real. o Override informa ao compilador que o elemento deve substituir um elemento declarado em uma superclasse
	@Override
	public double converterEmReal() {
		return valor * peso;
	}

}

package trabalho;

import java.util.Scanner;

//Minha unica duvida foi no metodo info

public class Principal {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		int opcao;

		Cofrinho cofrinho = new Cofrinho();

		System.out.println("-------------------------");
		System.out.println("1-Adicionar");
		System.out.println("2-Remover");
		System.out.println("3-Listar");
		System.out.println("4-Calcular Total em Reais");
		System.out.println("0-Encerrar");
		System.out.println("-------------------------");
		opcao = teclado.nextInt();
		System.out.println("--------------------------");

		int tipoMoeda;

		Moeda moeda;

		while (opcao != 0) {
			switch (opcao) {

			case 1:
				// Adicionar
				tipoMoeda = 0;
				while (tipoMoeda > 4 || tipoMoeda <= 0) {
					System.out.println("Escolha um tipo de Moeda!");
					System.out.println("1-Real");
					System.out.println("2-Dolar");
					System.out.println("3-Euro");
					tipoMoeda = teclado.nextInt();
					System.out.println("--------------------------");
				}

				moeda = null;
				if (tipoMoeda == 1) {
					System.out.println("Quanto você desejar Depositar? ");
					double valor = teclado.nextDouble();
					moeda = new Real(valor);
					System.out.println("---------------------------");
					System.out.println("Depositado com Sucesso!!!");

				}
				if (tipoMoeda == 2) {
					System.out.println("Quanto você desejar Depositar? ");
					double valor = teclado.nextDouble();
					moeda = new Dolar(valor);
					System.out.println("---------------------------");
					System.out.println("Depositado com Sucesso!!!");

				}
				if (tipoMoeda == 3) {
					System.out.println("Quanto você desejar Depositar? ");
					double valor = teclado.nextDouble();
					moeda = new Euro(valor);
					System.out.println("---------------------------");
					System.out.println("Depositado com Sucesso!!!");

				}

				cofrinho.adicionar(moeda);
				break;
			case 2:
				// Remover
				tipoMoeda = 0;
				while (tipoMoeda > 4 || tipoMoeda <= 0) {
					System.out.println("Escolha um tipo de Moeda!");
					System.out.println("1-Real");
					System.out.println("2-Dolar");
					System.out.println("3-Euro");
					tipoMoeda = teclado.nextInt();
					System.out.println("--------------------------");
				}

				moeda = null;
				if (tipoMoeda == 1) {
					System.out.println("Quanto você desejar Sacar? ");
					double valor = teclado.nextDouble();
					moeda = new Real(valor);
					System.out.println("---------------------------");
					System.out.println("Saque Efetuado com Sucesso!!!");

				}
				if (tipoMoeda == 2) {
					System.out.println("Quanto você desejar Sacar? ");
					double valor = teclado.nextDouble();
					moeda = new Dolar(valor);
					System.out.println("---------------------------");
					System.out.println("Saque Efetuado com Sucesso!!!");

				}
				if (tipoMoeda == 3) {
					System.out.println("Quanto você desejar Sacar? ");
					double valor = teclado.nextDouble();
					moeda = new Euro(valor);
					System.out.println("---------------------------");
					System.out.println("Saque Efetuado com Sucesso!!!");

				}

				cofrinho.remover(moeda);
				break;
			case 3:
				// Listar
				cofrinho.listar();
				break;
			case 4:
				// TotalConvertido
				cofrinho.totalConvertido();

				break;
			default:
				System.out.println("Opção Invalida");

			}
			System.out.println("-------------------------");
			System.out.println("1-Adicionar");
			System.out.println("2-Remover");
			System.out.println("3-Listar");
			System.out.println("4-Calcular Total em Reais");
			System.out.println("0-Encerrar");
			System.out.println("-------------------------");
			opcao = teclado.nextInt();
			System.out.println("--------------------------");

		}

	}

}
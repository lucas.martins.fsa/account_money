package trabalho;

public class Real extends Moeda {
	public String nome = "Real";

	public Real(double valor) {
		super(valor);
	}

	@Override
	public String toString() {
		return "Real - R$ " + valor + "";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Real other = (Real) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	public double converterEmReal() {
		return valor;
	}

}
